﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Endscript : MonoBehaviour {
    
	void Start () {
		
	}

	void Update () {
        if (Input.GetKey(KeyCode.Escape))
        {
            QuitGame();
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    public void SceneSwitcher(string newscene)
    {
        SceneManager.LoadScene(newscene);
    }
}
