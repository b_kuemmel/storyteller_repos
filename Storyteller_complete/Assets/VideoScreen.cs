﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VideoScreen : MonoBehaviour {
    
	void Start () {
	}
	
	void Update ()
    {
        SceneSwitcher("Endscreen");
    }

    public void VisibilityChanger(string changedobject)
    {
        GameObject.Find(changedobject).GetComponent<Canvas>().enabled = !GameObject.Find(changedobject).GetComponent<Canvas>().enabled;
    }

    public void SceneSwitcher(string newscene)
    {
        if (Input.GetKey(KeyCode.Return))
        {
            SceneManager.LoadScene(newscene);
        }
    }
}
